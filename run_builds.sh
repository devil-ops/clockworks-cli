#!/bin/bash
set -x
set -e

export CLOCKWORKS_BASE_URL=https://clockworks-test.oit.duke.edu

./scripts/clockworks-cli.py test-build rapid-ubuntu14 -f 000-9332
./scripts/clockworks-cli.py test-build rapid-ubuntu16 -f 000-9332


cowsay 'All tests completed'
