import os
from setuptools import setup, find_packages


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


with open("requirements.txt") as requirements_file:
    install_requirements = requirements_file.read().splitlines()


setup(
    name="clockworks-cli",
    version="1.0.23",
    author="Drew Stinnett",
    author_email="drew.stinnett@duke.edu",
    description=("Interact with clockworks in via the CLI"),
    install_requires=install_requirements,
    license="BSD",
    keywords="clockworks api cli",
    packages=find_packages(),
    scripts=["scripts/clockworks-cli.py"],
    long_description=read("README.md"),
)
