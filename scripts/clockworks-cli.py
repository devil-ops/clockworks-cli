#!/usr/bin/env python3
import argparse
import sys
import logging
from clockworkspy.helpers import (
    runner,
    my_import,
    get_clockworks_base,
    get_clockworks_api,
)
from clockworkspy import commands

clockworks_base = get_clockworks_base()
CLOCKWORKS = get_clockworks_api(clockworks_base)


def parse_arguments():
    """
    Handle argument parsing here
    """

    parser = argparse.ArgumentParser(
        description=" ".join(
            [
                "Command line application for the Clockworks application",
                "and some VMware operations",
            ]
        )
    )

    parser.add_argument("-v", "--verbose", action="store_true", help="Verbose output")
    subparsers = parser.add_subparsers(dest="action", help="sub-command help")
    subparsers.required = True

    # Loop though command files and add subparsers with them all
    for cmd in commands.__all__:
        # cmd_c = my_import('clockworkspy.commands.%s' % cmd).Command()
        mod = __import__("clockworkspy.commands", fromlist=[cmd])
        cmd_c = getattr(mod, cmd).Command()
        parser_sub = subparsers.add_parser(cmd, help=cmd_c.__doc__)

        if cmd_c.args_need_clockworks:
            cmd_c.add_arguments(parser_sub, clockworks=CLOCKWORKS)
        else:
            cmd_c.add_arguments(parser_sub)

    return parser.parse_args()


def main():

    args = parse_arguments()
    if args.verbose:
        default_log_level = logging.DEBUG
    else:
        default_log_level = logging.INFO
    logging.basicConfig(level=default_log_level)
    runner(args, CLOCKWORKS)
    return 0


if __name__ == "__main__":
    sys.exit(main())
