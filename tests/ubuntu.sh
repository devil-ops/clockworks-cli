#!/usr/bin/env bash

set -o errexit

DEPENDENCY_PACKAGES='python3 python3-pip python3-dev git gcc'

apt-get update
apt-get install -y ${DEPENDENCY_PACKAGES}

for package in ${DEPENDENCY_PACKAGES[@]}
do
  dpkg --verify $package
done

pip3 install -r requirements.txt

python3 setup.py install

cat << EOF > /${HOME}/.clockworks.yaml
---
'https://clockworks-test.oit.duke.edu':
  username: username
  apikey: foobarbaz
  apiprefix: /api/v1
'https://clockworks.oit.duke.edu':
  username: username
  apikey: foobarbaz
  apiprefix: /api/v1
EOF

clockworks-cli.py 2>&1 | grep 'Exception: Bad Credentials'
