#!/usr/bin/env bash

set -o errexit

DEPENDENCY_PACKAGES='python34 python34-pip python34-devel git gcc'

yum install -y epel-release
yum install -y ${DEPENDENCY_PACKAGES}

for package in ${DEPENDENCY_PACKAGES[@]}
do
  rpm -V $package
done

pip3 install -r requirements.txt

python3 setup.py install

cat << EOF > /${HOME}/.clockworks.yaml
---
'https://clockworks-test.oit.duke.edu':
  username: username
  apikey: foobarbaz
  apiprefix: /api/v1
'https://clockworks.oit.duke.edu':
  username: username
  apikey: foobarbaz
  apiprefix: /api/v1
EOF

clockworks-cli.py 2>&1 | grep 'Exception: Bad Credentials'
