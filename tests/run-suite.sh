#!/usr/bin/env bash

set -o errexit
set -o nounset

IMAGES=('ubuntu' 'centos' 'fedora')

for image in ${IMAGES[@]}
do
  sudo docker run --workdir /code \
    --volume $(pwd):/code \
    --security-opt="label=disable" \
    --rm \
    --name clockworks-cli-test \
    --entrypoint bash \
    -it ${image}:latest tests/${image}.sh
done
