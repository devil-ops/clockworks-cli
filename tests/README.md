Run Some Tests
==============

This really just tests that the deps and whatever install enough for the clockworks-cli.py to be able to init.  No fancy unit tests here.  Just make sure it can be installed on Fedora, Ubuntu and Centos.

Uses Docker.  Runs with sudo.  Check out the run-suite.sh for specifics.

To test:

`bash tests/run-suite.sh`

Use at your own risk.
