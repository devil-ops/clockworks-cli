# CLOCKWORKS-CLI

**THIS PROJECT IS DEPRECATED IN FAVOR OF [clockworksctl](https://gitlab.oit.duke.edu/devil-ops/clockworksctl). Please use that going forward**

Misc scripts and libraries to interact with the clockworks api, and also do some misc. VMware stuff using the information gathered from clockworks.

## Prerequisites

### Install directly from Gitlab

The --user flag will install it to your home directory and not require root privileges.

    ```
    pip3 install git+https://gitlab.oit.duke.edu/devil-ops/clockworks-cli.git --user
    ```

### Native Python Modules

Install required modules with

    ```
    pip3 install -r ./requirements.txt
    python3 ./setup.py install
    ```

## Using Pipenv

   ```
   pipenv install
   ```

## Setup

Make sure you have *~/.clockworks.yaml* set up like this:

```
---
'https://clockworks-test.oit.duke.edu':
  username: username
  apikey: $ lpass show clockworks-test-api --password
  apiprefix: /api/v1
'https://clockworks.oit.duke.edu':
  username: username
  apikey: $ lpass show clockworks-api --password
  apiprefix: /api/v1
```

The script will default to using 'https://clockworks-test.oit.duke.edu'.  If you would like to change it to production (or any other URL), set your environment variable

```
export CLOCKWORKS_BASE_URL=https://clockworks.oit.duke.edu
```

For vSphere operations, you'll also need a *~/.vmware.yaml* file, set up like this:

```
---
main:
  username: username
  password: $ lpass show duke.edu --password

guest:
  username: root
  password: $ lpass show 'duke root' --password
```

## Usage

```
Using env variable: https://clockworks.oit.duke.edu
usage: clockworks-cli.py [-h] [-v]
                         {vm-disk-list,vm-disk-list-vmware,test-build,container-list,vm-console,vm-reboot,vm-run-script,vm-info,network-list,vm-disk-add,location-list,vm-request-info,vm-disk-resize,vm-delete,os-list,vm-create,vm-poweron,vm-request-list,vm-list,vm-rebuild,vm-disk-resize-vmware,vm-edit}
                         ...

Command line application for the Clockworks application and some VMware
operations

positional arguments:
  {vm-disk-list,vm-disk-list-vmware,test-build,container-list,vm-console,vm-reboot,vm-run-script,vm-info,network-list,vm-disk-add,location-list,vm-request-info,vm-disk-resize,vm-delete,os-list,vm-create,vm-poweron,vm-request-list,vm-list,vm-rebuild,vm-disk-resize-vmware,vm-edit}
                        sub-command help
    vm-disk-list        List Disks assocatied with the host through Clockworks
    vm-disk-list-vmware
                        List Disks assocatied with the host through VMware
    test-build          Test a build of a host, then immediately delete it
    container-list      List Containers
    vm-console          Open up the VMware console
    vm-reboot           Reboot a VM using the VSphere API
    vm-run-script       Upload and run a script on a VM
    vm-info             Show information on a Clockworks VM
    network-list        List Networks
    vm-disk-add         Add a new disk to a VM
    location-list       List locations in the VMware cluster
    vm-request-info     Show information on a VM Request
    vm-disk-resize      Resize a disk through Clockworks
    vm-delete           Delete a VM
    os-list             List OSes
    vm-create           Create a new VM
    vm-poweron          Reboot a VM using the VSphere API
    vm-request-list     List VM Requests
    vm-list             List VMs
    vm-rebuild          Rebuild a VM
    vm-disk-resize-vmware
                        Resize a disk through VMware
    vm-edit             Add a new disk to a VM
    provision-list      List OS provisioning date

optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose         Verbose output
```

# More information

[Swagger Documentation](https://clockworks.oit.duke.edu/help/api_v1)

[API Token Generation](https://clockworks.oit.duke.edu/api_info)
