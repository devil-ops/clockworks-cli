from pyVmomi import vim
from pyVim.connect import SmartConnect, Disconnect
from devilparser import rcfile
import atexit


class vsphere(object):
    """
    Vsphere class
    """
    def __init__(self, servername, username, password, port=443):
        # Barf, these certs suck
        import ssl
        ssl._create_default_https_context
        ssl._create_default_https_context = ssl._create_unverified_context
        self.si = SmartConnect(
            host=servername,
            user=username,
            pwd=password,
            port=port)
        # disconnect this thing
        atexit.register(Disconnect, self.si)

    def si(self):
        return self.si

    def get_vm(self, vm_name):
        content = self.si.RetrieveContent()
        return self.get_obj(content, [vim.VirtualMachine], vm_name)

    def wait_for_power_state(self, vm_name, power_state, seconds=30):
        import time
        from datetime import datetime, timedelta
        vm = self.get_vm(vm_name)
        start = datetime.now()

        while True:
            if vm.runtime.powerState == power_state:
                return True
            else:
                time.sleep(1)
                waiting_for = datetime.now() - start
                if waiting_for > timedelta(seconds=seconds):
                    print("Waiting too long, continueing")
                    return False

    def poweroff_guest(self, vm_name):
        vm = self.get_vm(vm_name)
        if vm.runtime.powerState == 'poweredOff':
            print("Good news, %s is already powered off" % vm_name)
        else:
            vm.ShutdownGuest()
            self.wait_for_power_state(vm_name, 'poweredOff')

    def poweroff_vm(self, vm_name):
        import time
        vm = self.get_vm(vm_name)
        task = vm.PowerOff()
        while task.info.state not in [vim.TaskInfo.State.success,
                                      vim.TaskInfo.State.error]:
            time.sleep(1)
        self.wait_for_power_state(vm_name, 'poweredOff')

    def poweron_vm(self, vm_name):
        import time
        vm = self.get_vm(vm_name)
        task = vm.PowerOn()
        while task.info.state not in [vim.TaskInfo.State.success,
                                      vim.TaskInfo.State.error]:
            time.sleep(1)
        self.wait_for_power_state(vm_name, 'poweredOn')

    def print_vm_info(self, vm_name):
        vm = self.get_vm(vm_name)
        print("Found Virtual Machine")
        print("=====================")
        details = {
            'name': vm.summary.config.name,
            'instance UUID': vm.summary.config.instanceUuid,
            'bios UUID': vm.summary.config.uuid,
            'path to VM': vm.summary.config.vmPathName,
            'guest OS id': vm.summary.config.guestId,
            'guest OS name': vm.summary.config.guestFullName,
            'host name': vm.runtime.host.name,
            'last booted timestamp': vm.runtime.bootTime
        }

        for name, value in details.items():
            print("  {0:{width}{base}}: {1}".format(name, value, width=25,
                                                    base='s'))

        print("  Devices:")
        print("  --------")
        for device in vm.config.hardware.device:
            # diving into each device, we pull out a few interesting bits
            dev_details = {
                'key': device.key,
                'summary': device.deviceInfo.summary,
                'device type': type(device).__name__,
                'backing type': type(device.backing).__name__
            }

            print("  label: {0}".format(device.deviceInfo.label))
            print("  ------------------")
            for name, value in dev_details.items():
                print("    {0:{width}{base}}: {1}".format(name, value,
                                                          width=15,
                                                          base='s'))

            if device.backing is None:
                continue

            if hasattr(device.backing, 'fileName'):
                datastore = device.backing.datastore
                if datastore:
                    print("    datastore")
                    print("        name: {0}".format(datastore.name))
                    for host_mount in datastore.host:
                        host_system = host_mount.key
                        print("        host: {0}".format(host_system.name))
                    print("        summary")
                    summary = {
                        'capacity': datastore.summary.capacity,
                        'freeSpace': datastore.summary.freeSpace,
                        'file system': datastore.summary.type,
                        'url': datastore.summary.url
                    }
                    for key, val in summary.items():
                        print("            {0}: {1}".format(key, val))
                print("    fileName: {0}".format(device.backing.fileName))
                print("    device ID: {0}".format(
                    device.backing.backingObjectId))

        print("  ------------------")

    def get_disk_labels(self, vm_name):
        labels = []
        for disk in self.get_disks(vm_name):
            labels.append(disk.deviceInfo.label)
        return labels

    def upload_file(self, vm_name, src, dst):

        content = self.si.RetrieveContent()
        vm = self.get_vm(vm_name)
        guest_info = rcfile.parse('~/.vmware.yaml', 'guest').contents()
        creds = vim.vm.guest.NamePasswordAuthentication(
            username=guest_info['username'],
            password=guest_info['password'])

        f = open(src, 'r')
        f_content = f.read()
        f.close()

        file_attribute = vim.vm.guest.FileManager.FileAttributes()
        url = content.guestOperationsManager.fileManager.\
            InitiateFileTransferToGuest(vm, creds, dst, file_attribute,
                                        len(f_content), True)

        import requests
        resp = requests.put(url, data=f_content, verify=False)
        if not resp.status_code == 200:
            return None
        else:
            return True

    def run_command(self, vm_name, program, arguments=''):
        content = self.si.RetrieveContent()
        vm = self.get_vm(vm_name)
        guest_info = rcfile.parse('~/.vmware.yaml', 'guest').contents()
        creds = vim.vm.guest.NamePasswordAuthentication(
            username=guest_info['username'],
            password=guest_info['password'])
        pm = content.guestOperationsManager.processManager
        print(program, arguments)
        ps = vim.vm.guest.ProcessManager.ProgramSpec(programPath=program,
                                                     arguments=arguments)

        res = pm.StartProgramInGuest(vm, creds, ps)

        return res

    def get_disks(self, vm_name):
        vm = self.get_vm(vm_name)
        disks = []
        for device in vm.config.hardware.device:
            # We just care about virtual disks here
            if type(device).__name__ != "vim.vm.device.VirtualDisk":
                continue

            disks.append(device)
        return disks

    def print_disk(self, device):
        print(device.deviceInfo.label)
        dev_details = {
            'key': device.key,
            'summary': device.deviceInfo.summary,
            'backing type': type(device.backing).__name__}

        for name, value in dev_details.items():
            print("    {0:{width}{base}}: {1}".format(name, value, width=15,
                                                      base='s'))
            print()

    def resize_disk(self, vm_name, disk_label, new_size):
        """
        Given vm_name, disk_label and size in GB, resize a disk
        """
        vm = self.get_vm(vm_name)
        for dev in vm.config.hardware.device:
            if hasattr(dev.backing, 'fileName'):
                if str(dev.deviceInfo.label) == disk_label:
                    capacity_in_kb = dev.capacityInKB
                    new_disk_kb = int(new_size) * 1024 * 1024

                    if new_disk_kb > capacity_in_kb:
                        dev_changes = []
                        disk_spec = vim.vm.device.VirtualDeviceSpec()
                        disk_spec.operation = vim.vm.device\
                            .VirtualDeviceSpec.Operation.edit
                        disk_spec.device = vim.vm.device.VirtualDisk()
                        disk_spec.device.key = dev.key
                        disk_spec.device.backing = vim.vm.device.VirtualDisk\
                            .FlatVer2BackingInfo()
                        disk_spec.device.backing.fileName = dev.backing\
                            .fileName
                        disk_spec.device.backing.diskMode = dev.backing\
                            .diskMode
                        disk_spec.device.controllerKey = dev.controllerKey
                        disk_spec.device.unitNumber = dev.unitNumber
                        disk_spec.device.capacityInKB = new_disk_kb
                        dev_changes.append(disk_spec)

                        spec = vim.vm.ConfigSpec()
                        spec.deviceChange = dev_changes

                        print("Resizing %s to %sGB" % (disk_label, new_size))

    def add_disk(self, vm_name, disk_size, disk_type='thin'):
        """
        Add a disk in GB to a VM
        """
        spec = vim.vm.ConfigSpec()
        # get all disks on a VM, set unit_number to the next available
        unit_number = 0
        vm = self.get_vm(vm_name)
        for dev in vm.config.hardware.device:
            if hasattr(dev.backing, 'fileName'):
                unit_number = int(dev.unitNumber) + 1
                # unit_number 7 reserved for scsi controller
                if unit_number == 7:
                    unit_number += 1
                if unit_number >= 16:
                    print("we don't support this many disks")
                    return
            if isinstance(dev, vim.vm.device.VirtualSCSIController):
                controller = dev
        # add disk here
        dev_changes = []
        new_disk_kb = int(disk_size) * 1024 * 1024
        disk_spec = vim.vm.device.VirtualDeviceSpec()
        disk_spec.fileOperation = "create"
        disk_spec.operation = vim.vm.device.VirtualDeviceSpec.Operation.add
        disk_spec.device = vim.vm.device.VirtualDisk()
        disk_spec.device.backing = \
            vim.vm.device.VirtualDisk.FlatVer2BackingInfo()
        if disk_type == 'thin':
            disk_spec.device.backing.thinProvisioned = True
        disk_spec.device.backing.diskMode = 'persistent'
        disk_spec.device.unitNumber = unit_number
        disk_spec.device.capacityInKB = new_disk_kb
        disk_spec.device.controllerKey = controller.key
        dev_changes.append(disk_spec)
        spec.deviceChange = dev_changes
        vm.ReconfigVM_Task(spec=spec)
        print("%sGB disk added to %s" % (disk_size, vm.config.name))

    def get_obj(self, content, vimtype, name):
        obj = None
        container = content.viewManager.CreateContainerView(
            content.rootFolder, vimtype, True)
        for c in container.view:
            if c.name == name:
                obj = c
                break
        return obj


def add_disk(vm, si, disk_size, disk_type):
        spec = vim.vm.ConfigSpec()
        # get all disks on a VM, set unit_number to the next available
        unit_number = 0
        for dev in vm.config.hardware.device:
            if hasattr(dev.backing, 'fileName'):
                unit_number = int(dev.unitNumber) + 1
                # unit_number 7 reserved for scsi controller
                if unit_number == 7:
                    unit_number += 1
                if unit_number >= 16:
                    print("we don't support this many disks")
                    return
            if isinstance(dev, vim.vm.device.VirtualSCSIController):
                controller = dev
        # add disk here
        dev_changes = []
        new_disk_kb = int(disk_size) * 1024 * 1024
        disk_spec = vim.vm.device.VirtualDeviceSpec()
        disk_spec.fileOperation = "create"
        disk_spec.operation = vim.vm.device.VirtualDeviceSpec.Operation.add
        disk_spec.device = vim.vm.device.VirtualDisk()
        disk_spec.device.backing = \
            vim.vm.device.VirtualDisk.FlatVer2BackingInfo()
        if disk_type == 'thin':
            disk_spec.device.backing.thinProvisioned = True
        disk_spec.device.backing.diskMode = 'persistent'
        disk_spec.device.unitNumber = unit_number
        disk_spec.device.capacityInKB = new_disk_kb
        disk_spec.device.controllerKey = controller.key
        dev_changes.append(disk_spec)
        spec.deviceChange = dev_changes
        vm.ReconfigVM_Task(spec=spec)
        print("%sGB disk added to %s" % (disk_size, vm.config.name))
