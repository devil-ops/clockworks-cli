import logging
import sys


class Api(object):
    """
    API Object
    """

    @classmethod
    def __init__(self, clockworks_base, credentials):
        self.clockworks_base = clockworks_base
        self.credentials = (credentials["username"], credentials["password"])

    def get_vmware_server_from_mob_url(self, mob_url):
        """
        Given a mob_url (https://vmware-test.oit.duke.edu/mob/?moid=vm-6252),
        return the vmware server (vmware-test.oit.duke.edu)
        """
        return mob_url.split("/")[2]

    def create_vm(
        self,
        hostname,
        container,
        fund_code,
        os_id,
        template,
        network,
        location_id,
        hosting_level,
        backups=False,
        cpu=1,
        ram=1,
        storage_size=50,
        sensitive_data=False,
        monitoring=False,
        sysadmin="8x5",
        disaster_recovery=False,
        production_level="development",
        patch_window="any",
        cloud_init=None,
        disks=[],
        application_name="",
    ):
        """
        Create a VM, yay!!
        """

        if network in ["public", "private"]:
            network_type = network
            subnet = None
        else:
            network_type = "custom"
            subnet = network

        sysadmin_option_id = self.get_sysadmin_aliases()[sysadmin]
        patch_window_id = self.get_patchwindow_aliases()[patch_window]
        print("DEBUG: %s" % os_id)

        data = {
            "location_id": location_id,
            "hostname": hostname,
            "os_id": os_id,
            "fund_code": fund_code,
            "network_type": network_type,
            "sysadmin_option_id": sysadmin_option_id,
            "container_name": container,
            "ram": ram,
            "cpu": cpu,
            "production_level": production_level,
            "hosting_level": hosting_level,
            "backups": backups,
            "disaster_recovery": disaster_recovery,
            "patch_window_id": patch_window_id,
            "application_name": application_name,
        }

        if len(disks) > 0:
            data["disks"] = disks

        if cloud_init:
            data["cloud_init_user_data"] = cloud_init

        if subnet:
            data["subnet"] = subnet

        print("DATA: %s" % data)

        return self.query_path("vms", method="post", data=data)

    @classmethod
    def print_errors(cls, errors):
        """
        Print errors array from an clockworks api call
        """
        for item in errors:
            logging.error(item)

    @classmethod
    def print_notices(cls, notices):
        """
        Print notices array from an clockworks api call
        """
        for notice in notices:
            logging.info(notice)

    # TODO:  Change this to use a post filter
    def get_vm_from_name(self, name):
        filtered_vms = self.query_path("vms?name=%s" % name)
        vms = []
        for filtered_vm in filtered_vms:
            if filtered_vm["name"] == name:
                vms.append(filtered_vm)
        if len(vms) != 1:
            raise Exception("Got %s results for %s instead of 1" % (len(vms), name))
        return vms[0]

    def list_location_ids(self):
        items = []
        for item in self.query_path("locations"):
            items.append(item["id"])
        return items

    def list_location_names(self):
        items = []
        for item in self.query_path("locations"):
            items.append(item["display_name"])
        return items

    def list_locations(self):
        items = {}
        for item in self.query_path("locations"):
            items[item["display_name"]] = item["id"]
        return items

    def list_container_names(self):
        items = []
        for item in self.query_path("containers"):
            items.append(item["name"])
        return items

    def list_patchwindow_names(self):
        items = []
        for item in self.query_path("patch_windows"):
            items.append(item["name"])
        return items

    def get_patchwindow_aliases(self):
        items = {}
        for item in self.query_path("patch_windows"):
            items[item["name"]] = item["id"]
        return items

    #       return {
    #           "any": -1,
    #           "mon8a-patchinfra": 10,
    #           "wed3a": 2,
    #           "wed8a": 3,
    #           "wed1p-tsm": 12,
    #           "wed7p": 1,
    #           "thu3a": 5,
    #           "thu8a": 6,
    #           "thu7p": 4,
    #           "sat3a": 8,
    #           "sat6a-siss": 11,
    #           "sat8a": 9,
    #           "sat7p": 7,
    #           "thu2p-laborworkx": 13,
    #           "tue6p-emergency": 14,
    #           "wed8a-dupd1": 15,
    #           "thu8a-dupd2": 16,
    #           "sun3a-aleph": 17,
    #       }

    def list_os_ids(self):
        """
        Return a list of os_ids
        """
        os_ids = []
        for item in self.query_path("oses"):
            os_ids.append(item["os_id"])
        return os_ids

    def get_os_info_from_name(self, os_name):
        os_id = self.get_os_id_aliases()[os_name]
        return self.query_path("oses/%s" % os_id)

    def get_requests_for_hostname(self, hostname, exclude_statuses=["done"]):
        # This old way to query requests was too intensive, use
        # get_request_for_host_id instead
        raise NotImplementedError

    def get_requests_for_host_id(self, host_id, exclude_statuses=["done"]):
        requests = []
        for item in self.query_path("/vms/%s/vm_requests" % host_id):
            if item["status"] not in exclude_statuses:
                requests.append(item)
        return requests

    def get_os_info_from_id(self, os_id):
        return self.query_path("oses/%s" % os_id)

    def get_sysadmin_aliases(self):
        return {
            "selfadmin": 1,
            "8x5": 2,
            "24x7": 3,
            "8x5-web": 4,
            "24x7-web": 5,
        }

    def get_os_id_aliases(self):
        """
        Helper method to get some more human friendly names to os_ids
        """
        os_info = {
            "rapid-ubuntu16": "rapid-ubuntu16",
            "rapid-ubuntu14": "rapid-ubuntu14",
            "selfadmin-ubuntu16": "ubuntu16",
            "selfadmin-ubuntu14": "ubuntu14",
            "ssi-rhel7": "ssiRHEL7",
            "ssi-rhel6": "ssiRHEL6",
        }
        return os_info

    def wait_for_request_to_complete(self, request_id):
        from progress.spinner import Spinner

        completed = None
        previous_status = None
        logging.info("Waiting for Request to complete...")
        while not completed:
            response = self.query_path("vm_requests/%s" % request_id)
            status = response["status"]

            # If it busts
            if status in ["error"]:
                return None

            # Good exists here
            if status in ["done"]:
                print()
                return True
            else:
                if status != previous_status:
                    spinner = Spinner("\n%s " % status)
                if logging.getLogger().getEffectiveLevel() > 10:
                    spinner.next()
                previous_status = status

    @classmethod
    def query_path(self, path, data=None, method="get"):
        """
        Do the actual API query here
        """
        import requests

        url = "%s/api/v1/%s" % (self.clockworks_base, path)
        logging.debug("URL: %s PARAMS: %s METHOD: %s", url, data, method)

        headers = {"Content-Type": "application/json", "Accept": "application/json"}

        if method == "get":
            result = requests.get(url, auth=self.credentials, headers=headers)
        elif method == "put":
            result = requests.put(url, auth=self.credentials, data=data)
        elif method == "post":
            c_headers = ['"{0}: {1}"'.format(k, v) for k, v in headers.items()]
            c_headers = " -H ".join(c_headers)
            result = requests.post(url, auth=self.credentials, json=data)
        elif method == "delete":
            result = requests.delete(url, auth=self.credentials, data=data)
        else:
            logging.error("Invalid method: %s", method)
            sys.exit(2)

        try:
            real_result = result.json()
        except BaseException:
            real_result = result

            if real_result.status_code == 401:
                raise Exception("Bad Credentials")

        if "error" in real_result:
            logging.error("Error in request, quitting: %s", real_result["error"])
            sys.exit(2)

        if "notices" in real_result:
            self.print_notices(real_result["notices"])

        if "errors" in real_result:
            if len(real_result["errors"]) > 0:
                self.print_errors(real_result["errors"])
                logging.error("Quitting because of errors")
                sys.exit(2)

        return real_result


class SubCommand(object):
    def __init__(self):
        self.args_need_clockworks = None
        self.commands_need_vmware = None

    def add_arguments(self, args):
        return None

    def add_complex_arguments(self, args, CLOCKWORKS=None):
        return None
