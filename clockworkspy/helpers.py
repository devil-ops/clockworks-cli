"""
Helper functions
"""
import os
import sys
from devilparser import rcfile
from clockworkspy.clockworks import Api as ClockworksApi

# TODO:  This is ugly, figure out a better way
# from clockworkspy.commands import *


class CredentialParser(object):
    """
    Import credentials for connecting to Clockworks

    This file should look something like:
    [https://clockworks-test.oit.duke.edu]
    username = 'username'
    apikey = 'XXXX'
    apiprefix = '/api/v1/vm_api'
    """

    def __init__(self, base_url):
        self.base_url = base_url

    def import_credentials(self):
        """
        Pull in your credentials
        """
        cred_file = os.path.expanduser("~/.clockworksrc")
        if not os.path.exists(cred_file):
            sys.exit("%s is required" % cred_file)
        import configparser

        config = configparser.ConfigParser()
        config.read(cred_file)

        creds = {}
        creds["username"] = config.get(self.base_url, "username")
        creds["password"] = config.get(self.base_url, "apikey")
        return creds


def pp_results(results):
    """
    Print list of results (or just results)
    """
    if isinstance(results, list):
        for result in results:
            pp_result(result)
    else:
        pp_result(results)


def pp_result(result):
    """
    Print out json results in a pretty way (Pretty Print)
    """
    from datetime import timedelta
    import humanize

    for name, value in result.items():

        # Skip empty junk
        if not value:
            continue

        if name == "ttl":
            natty_time = humanize.naturaltime(timedelta(seconds=int(value))).replace(
                " ago", ""
            )
            hr_value = "%s (or %s seconds)" % (natty_time, value)
        elif name == "last_registered":
            hr_value = naturalize_day(value)
        elif name == "last_unregistered":
            hr_value = naturalize_day(value)
        elif name == "last_seen":
            hr_value = naturalize_day(value)
        else:
            hr_value = value
        print("%-20s %-5s" % (name, hr_value))


def naturalize_day(value):
    """
    Given an ISO date, return something human readable
    """
    import humanize
    from dateutil import parser

    try:
        natty_time = humanize.naturalday(parser.parse(value))
    except BaseException:
        natty_time = value
    hr_value = "%s (%s)" % (natty_time, value)
    return hr_value


def my_import(name):
    components = name.split(".")
    mod = __import__(components[0])
    for comp in components[1:]:
        mod = getattr(mod, comp)
    return mod


def get_clockworks_base():
    if "CLOCKWORKS_BASE_URL" in os.environ.keys():
        print("Using env variable: %s" % os.environ["CLOCKWORKS_BASE_URL"])
        clockworks_base = os.environ["CLOCKWORKS_BASE_URL"]
    else:
        print("Using default clockworks-test")
        clockworks_base = "https://clockworks-test.oit.duke.edu"
    return clockworks_base


def get_clockworks_api(clockworks_base):
    configinfo = rcfile.parse("~/.clockworks.yaml", clockworks_base).contents()
    cred = {}
    cred["username"] = configinfo["username"]
    cred["password"] = configinfo["apikey"]
    return ClockworksApi(clockworks_base, cred)


def runner(args, clockworks):
    """
    Wrapper to do all the actual commands
    """

    if not args.action:
        sys.stderr.write("Invalid usage, use -h\n")
        sys.exit(2)
    # cmd = my_import("clockworkspy.commands.%s" % args.action).Command()
    # cmd.run(args, clockworks)
    mod = __import__("clockworkspy.commands", fromlist=[args.action])
    cmd = getattr(mod, args.action).Command()
    cmd.run(args, clockworks)
