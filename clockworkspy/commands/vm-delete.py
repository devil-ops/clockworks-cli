from clockworkspy.clockworks import SubCommand
from clockworkspy.helpers import pp_results
import dateparser
import sys


class Command(SubCommand):
    """
    Delete a VM
    """

    def add_arguments(self, parser):
        parser.add_argument("target", type=str, help="Entry to delete")
        parser.add_argument(
            "--archive-delete-time",
            type=str,
            default="30 days",
            help="Delete archive after...",
        )
        parser.add_argument(
            "--no-confirm",
            action="store_true",
            help="Do not ask for confirmation when deleting",
        )
        return parser

    def run(self, args, CLOCKWORKS):
        info = CLOCKWORKS.get_vm_from_name(args.target)
        archive_delete_date = dateparser.parse(args.archive_delete_time)
        if not archive_delete_date:
            sys.stderr.write(
                "Could not convert delete time to a valid datetime: %s\n"
                % args.archive_delete_time
            )
            sys.stderr.write(
                "Check out the docs at https://pypi.org/project/dateparser/\n"
            )
            return 5
        archive_delete_after = archive_delete_date.isoformat()
        print("About to delete %s" % args.target)
        print("Will delete archive at %s" % archive_delete_after)
        if not args.no_confirm:
            input("Press Enter to continue...")

        # See if any requests are still pending
        vm_data = CLOCKWORKS.get_vm_from_name(args.target)
        for request in CLOCKWORKS.get_requests_for_host_id(vm_data["id"]):
            if request["status"] == "error":

                print("TRYING TO KILL REQUEST %s" % request["sn_request_id"])
                try:
                    from servicenowpy.servicenow import Api as SNApi
                    from devilparser import rcfile
                except BaseException:
                    sys.stderr.write("No servicenowpy module installed :(\n")
                    continue

                # TODO:  Move this outside somewhere
                configinfo = rcfile.parse("~/.servicenow.yaml", "duke").contents()
                cred = {}
                cred["username"] = configinfo["username"]
                cred["password"] = configinfo["password"]
                SN = SNApi("%s.service-now.com" % "duke", cred)

                req = SNApi.query_table(
                    "sc_request?number=%s" % request["sn_request_id"]
                )
                sys_id = req[0]["sys_id"]
                for task in SN.get_tasks_from_req(sys_id):
                    print("Marking %s as skipped" % task["number"])
                    closed_skipped = SN.get_state_id("task", "Closed Skipped")
                    res = SN.query_table(
                        "task/%s" % task["sys_id"],
                        method="put",
                        data={"state": str(closed_skipped)},
                    )

        res = CLOCKWORKS.query_path(
            "vms/%s" % info["id"],
            method="delete",
            data={"archive_delete_time": archive_delete_after},
        )
        pp_results(res)
