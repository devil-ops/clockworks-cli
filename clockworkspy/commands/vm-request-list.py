from clockworkspy.clockworks import SubCommand
from clockworkspy.helpers import pp_results


class Command(SubCommand):
    """
    List VM Requests
    """

    def add_arguments(self, parser):
        return None

    def run(self, args, CLOCKWORKS):
        for item in CLOCKWORKS.query_path('vm_requests'):
            pp_results(item)
            print()
