from clockworkspy.clockworks import SubCommand
import sys
import platform
import os


class Command(SubCommand):
    """
    Open up the VMware console
    """

    def add_arguments(self, parser):
        parser.add_argument('target', help='Hostname of the VM')

    def run(self, args, CLOCKWORKS):
        supported_platforms = ['Darwin']

        if platform.system() not in supported_platforms:
            sys.exit("Not supported on %s, submit a patch though!"
                     % platform.system())

        from urllib.parse import urlparse, parse_qs
        mob_url = CLOCKWORKS.get_vm_from_name(args.target)['mob_url']
        parts = urlparse(mob_url)

        console_url = 'vmrc://%s:443/?moid=%s' % (
            parts.netloc, parse_qs(parts.query)['moid'][0])

        console_cmd = "open '%s'" % console_url
        print("Opening console with: %s" % console_cmd)
        os.system("open '%s'" % console_url)
