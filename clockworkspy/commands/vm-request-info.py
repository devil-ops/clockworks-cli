from clockworkspy.clockworks import SubCommand
from clockworkspy.helpers import pp_results


class Command(SubCommand):
    """
    Show information on a VM Request
    """

    def add_arguments(self, parser):
        parser.add_argument('target', type=int,
                            help='VM Request ID to query')

    def run(self, args, CLOCKWORKS):
        pp_results(CLOCKWORKS.query_path('vm_requests/%s' % args.target))
