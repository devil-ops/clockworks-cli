from clockworkspy.clockworks import SubCommand
from devilparser import rcfile
from clockworkspy.vmware import vsphere


class Command(SubCommand):
    """
    Add a new disk to a VM
    """

    def add_arguments(self, parser):
        parser.add_argument(
            'target',
            help='Hostname of the VM that the disk will be added to')
        parser.add_argument('size', type=int,
                            help='Size in Gb of the new disk')
        return parser

    def run(self, args, CLOCKWORKS):
        vm = CLOCKWORKS.get_vm_from_name(args.target)
        print("Adding %sGB to %s" % (args.size, args.target))
        vmware_server = CLOCKWORKS.get_vmware_server_from_mob_url(
            vm['mob_url'])
        vmware_info = rcfile.parse('~/.vmware.yaml').contents()
        VSPHERE = vsphere(vmware_server, vmware_info['username'],
                          vmware_info['password'])
        VSPHERE.add_disk(args.target, args.size)
