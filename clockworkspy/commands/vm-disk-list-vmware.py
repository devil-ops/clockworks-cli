from clockworkspy.clockworks import SubCommand
from devilparser import rcfile
from clockworkspy.vmware import vsphere


class Command(SubCommand):
    """
    List Disks assocatied with the host through VMware
    """
    def __init__(self):
        SubCommand.__init__(self)
        self.commands_need_vmware = True

    def add_arguments(self, parser):
        parser.add_argument('target', help='Hostname', nargs='+')
        return parser

    def run(self, args, CLOCKWORKS):

        for target in args.target:
            print("Retrieving disks for %s" % target)
            vm = CLOCKWORKS.get_vm_from_name(target)
            vmware_server = CLOCKWORKS.get_vmware_server_from_mob_url(
                vm['mob_url'])
            vmware_info = rcfile.parse('~/.vmware.yaml').contents()
            VSPHERE = vsphere(vmware_server, vmware_info['username'],
                              vmware_info['password'])
            for disk in VSPHERE.get_disks(target):
                VSPHERE.print_disk(disk)
