from clockworkspy.clockworks import SubCommand
from clockworkspy.helpers import pp_results


class Command(SubCommand):
    """
    List Patch Windows
    """
    def __init__(self):
        SubCommand.__init__(self)

    def add_arguments(self, parser):
        return None

    def run(self, args, CLOCKWORKS):
        for item in CLOCKWORKS.query_path('patch_windows'):
            pp_results(item)
            print()
