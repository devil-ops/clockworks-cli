from clockworkspy.clockworks import SubCommand
from devilparser import rcfile
from clockworkspy.vmware import vsphere
import sys


class Command(SubCommand):
    """
    Resize a disk through VMware
    """

    def add_arguments(self, parser):
        parser.add_argument(
            'target',
            help='Hostname of the VM that the disk will be added to')
        parser.add_argument(
            'disk',
            help='Disk to resize (use vm-list-disks to see list disks)')
        parser.add_argument('size', type=int,
                            help='Size in Gb of the new disk')
        return parser

    def run(self, args, CLOCKWORKS):
        vm = CLOCKWORKS.get_vm_from_name(args.target)
        vmware_server = CLOCKWORKS.get_vmware_server_from_mob_url(
            vm['mob_url'])
        vmware_info = rcfile.parse('~/.vmware.yaml').contents()
        VSPHERE = vsphere(vmware_server, vmware_info['username'],
                          vmware_info['password'])
        existing_disks = VSPHERE.get_disk_labels(args.target)
        if args.disk not in existing_disks:
            sys.stderr.write("%s is not a valid disk\n" % args.disk)
            sys.stderr.write("Available disks are: %s\n" % existing_disks)
            sys.exit(2)
        VSPHERE.resize_disk(args.target, args.disk, args.size)
