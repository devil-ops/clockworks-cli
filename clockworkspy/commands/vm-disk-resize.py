from clockworkspy.clockworks import SubCommand
import json


class Command(SubCommand):
    """
    Resize a disk through Clockworks
    """

    def add_arguments(self, parser):
        parser.add_argument(
            'target',
            help='Hostname of the VM that the disk will be added to')
        parser.add_argument(
            'disk', type=int,
            help='Disk ID to resize (use vm-list-disks to see list disks)')
        parser.add_argument('size', type=int,
                            help='Size in Gb of the new disk')
        parser.add_argument('-w', '--wait', action='store_true',
                            help='Wait for request to complete')
        return parser

    def run(self, args, CLOCKWORKS):
        vm = CLOCKWORKS.get_vm_from_name(args.target)
        payload = {}
        payload['disks'] = json.dumps([
            {"disk_id": args.disk, "size_gb": args.size},
        ])
        request = CLOCKWORKS.query_path('vms/%s' % vm['id'], method='put',
                                        data=payload)
        if args.wait:
            CLOCKWORKS.wait_for_request_to_complete(request['vm_request_id'])
            print("Completed change!")
        else:
            print("Request submitted: %s" % request['vm_request_id'])
