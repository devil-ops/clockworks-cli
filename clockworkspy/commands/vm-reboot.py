from clockworkspy.clockworks import SubCommand
from devilparser import rcfile
from clockworkspy.vmware import vsphere
import sys


class Command(SubCommand):
    """
    Reboot a VM using the VSphere API
    """

    def add_arguments(self, parser):
        reboot_method_choices = ['hard', 'soft', 'mccardle']
        parser.add_argument('target', help='Hostname of the VM to reboot',
                            nargs='+')
        parser.add_argument(
            '-m', '--method', choices=reboot_method_choices, default='soft',
            help=" ".join(
                ['Method for reboot.  Hard: VM reset, Soft: Guest reboot, ',
                 'Mccardle: Shutdown all VMs in the cluster']))
        return parser

    def run(self, args, CLOCKWORKS):
        for target in args.target:
            vm = CLOCKWORKS.get_vm_from_name(target)
            vmware_server = CLOCKWORKS.get_vmware_server_from_mob_url(
                vm['mob_url'])
            vmware_info = rcfile.parse('~/.vmware.yaml').contents()
            VSPHERE = vsphere(vmware_server, vmware_info['username'],
                              vmware_info['password'])
            if args.method == 'hard':
                print("Hard Powering off %s" % target)
                VSPHERE.poweroff_vm(target)
            elif args.method == 'soft':
                print("Soft Powering off %s" % target)
                VSPHERE.poweroff_guest(target)
            elif args.method == 'mcardle':
                print("Don't even play")
                sys.exit(2)

            print("Powering on %s" % target)
            VSPHERE.poweron_vm(target)
