from clockworkspy.clockworks import SubCommand
import argparse
import sys
import yaml


class Command(SubCommand):
    """
    Create a new VM
    """

    def __init__(self):
        SubCommand.__init__(self)
        self.args_need_clockworks = True

    def add_arguments(self, parser, clockworks):
        sysadmin_names = clockworks.get_sysadmin_aliases().keys()
        os_ids = clockworks.list_os_ids()
        os_names = clockworks.get_os_id_aliases().keys()
        container_names = clockworks.list_container_names()
        patchwindow_names = clockworks.get_patchwindow_aliases().keys()
        locations = clockworks.list_locations()

        parser.add_argument("hostname", type=str, help="Entry to create", nargs="+")
        os_group = parser.add_mutually_exclusive_group(required=True)
        os_group.add_argument("--os-id", type=str, choices=os_ids, help="OS ID to use")
        os_group.add_argument(
            "--os-name", type=str, choices=os_names, help="OS Name to use"
        )

        parser.add_argument(
            "-f", "--fund-code", required=True, help="Fund Code for server"
        )

        network_group = parser.add_mutually_exclusive_group(required=True)
        network_group.add_argument(
            "-s", "--subnet", type=str, dest="network", help="Subnet for VM.  Use CIDR"
        )
        network_group.add_argument(
            "--public",
            action="store_const",
            const="public",
            dest="network",
            help="Put VM on a Public Network",
        )
        network_group.add_argument(
            "--private",
            action="store_const",
            const="private",
            dest="network",
            help="Put VM on a Private Network",
        )
        parser.add_argument(
            "-w", "--wait", action="store_true", help="Wait for request to complete"
        )
        parser.add_argument(
            "-o",
            "--hosting-level",
            choices=["platinum", "silver", "bronze", "copper"],
            help="Hosting Level",
        )
        parser.add_argument(
            "-n",
            "--container",
            type=str,
            choices=container_names,
            help="Container for VM",
        )
        parser.add_argument(
            "-P",
            "--patch-window",
            type=str,
            default="Any - outside of business hours",
            choices=patchwindow_names,
            help="Patch Window",
        )
        parser.add_argument(
            "-c", "--cpu-count", type=int, default=1, help="Number of CPUs for VM"
        )
        parser.add_argument(
            "-m", "--memory", type=int, default=1, help="Amount of Memory for VM in Gb"
        )
        parser.add_argument(
            "-d", "--disk-size", type=int, default=50, help="Disk size in Gb", nargs="+"
        )
        parser.add_argument(
            "-b",
            "--backups",
            action="store_true",
            help="Also request backups for this VM",
        )
        parser.add_argument(
            "-u",
            "--cloud-init-user-data",
            type=argparse.FileType("r"),
            help="Use the given file for cloud init",
        )
        parser.add_argument(
            "-i",
            "--disaster-recovery",
            action="store_true",
            help="Also request disaster recovery for this VM",
        )
        parser.add_argument(
            "-e",
            "--sensitive-data",
            action="store_true",
            help="Sensitive Data is on this VM",
        )
        parser.add_argument(
            "-r",
            "--remove",
            action="store_true",
            help="Remove this VM after it is created [for testing builds]",
        )
        parser.add_argument(
            "-a",
            "--sysadmin",
            type=str,
            choices=sysadmin_names,
            default="8x5",
            help="Sysadmin Support",
        )
        parser.add_argument(
            "-p",
            "--production-level",
            type=str,
            choices=["production", "testing", "development"],
            help="Production Level",
            default="development",
        )

        parser.add_argument(
            "--application-name",
            type=str,
            help='Application Name for the CMDB'
        )

        default_location = None

        parser.add_argument(
            "-l",
            "--location",
            type=str,
            choices=locations.keys(),
            help="Location for VM",
            default=default_location,
            required=True,
        )
        parser.set_defaults(network="private")

    def generate_additional_disks_block(self, disk_sizes):
        target = []
        for disk_size in disk_sizes:
            item = {"size_gb": int(disk_size)}
            target.append(item)
        return target

    def run(self, args, CLOCKWORKS):

        # We only need to read this once
        cloud_init = None
        locations = CLOCKWORKS.list_locations()
        if args.cloud_init_user_data:
            cloud_init = args.cloud_init_user_data.read()

        for hostname in args.hostname:
            print("Creating %s" % hostname)
            if args.os_id:
                os_info = CLOCKWORKS.get_os_info_from_id(args.os_id)
            else:
                os_info = CLOCKWORKS.get_os_info_from_name(args.os_name)

            if cloud_init:
                try:
                    yaml.load(cloud_init)
                except Exception as e:
                    sys.stderr.write("%s\n" % e)
                    sys.stderr.write("Bad yaml in your cloud init file\n")
                    return 5

            request = CLOCKWORKS.create_vm(
                hostname=hostname,
                container=args.container,
                fund_code=args.fund_code,
                disks=self.generate_additional_disks_block(args.disk_size),
                os_id=os_info["os_id"],
                template=os_info["template"],
                network=args.network,
                ram=args.memory,
                hosting_level=args.hosting_level,
                production_level=args.production_level,
                backups=args.backups,
                cpu=args.cpu_count,
                location_id=locations[args.location],
                sysadmin=args.sysadmin,
                patch_window=args.patch_window,
                cloud_init=cloud_init,
                application_name=args.application_name
            )
            if args.wait:
                CLOCKWORKS.wait_for_request_to_complete(request["vm_request_id"])
                print("Completed change!")
            else:
                print("Request created: %s" % request["vm_request_id"])
