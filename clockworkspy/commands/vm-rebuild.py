from clockworkspy.clockworks import SubCommand


class Command(SubCommand):
    """
    Rebuild a VM
    """

    def add_arguments(self, parser):
        parser.add_argument('target', help='Hostname to rebuild')
        parser.add_argument('-w', '--wait', action='store_true',
                            help='Wait for request to complete')

    def run(self, args, CLOCKWORKS):
        vm = CLOCKWORKS.get_vm_from_name(args.target)
        request = CLOCKWORKS.query_path('vms/%s/rebuild' % vm['id'],
                                        method='put')
        if args.wait:
            CLOCKWORKS.wait_for_request_to_complete(request['vm_request_id'])
            print("Completed change!")
        else:
            print("Request created: %s" % request['vm_request_id'])
