from clockworkspy.clockworks import SubCommand
from clockworkspy.helpers import pp_results


class Command(SubCommand):
    """
    List locations in the VMware cluster
    """

    def add_arguments(self, parser):
        return None

    def run(self, args, CLOCKWORKS):
        for location in CLOCKWORKS.query_path('locations'):
            pp_results(location)
            print()
