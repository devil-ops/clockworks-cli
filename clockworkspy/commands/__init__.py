# Register command plugin instead of trying to autodetect them.  This works
# better with more recent python version
__all__ = [
    'version',
    "container-list",
    "location-list",
    "network-list",
    "os-list",
    "patchwindow-list",
    "provision-list",
    "test-build",
    "vm-console",
    "vm-create",
    "vm-delete",
    "vm-disk-add",
    "vm-disk-add-vmware",
    "vm-disk-list",
    "vm-disk-list-vmware",
    "vm-disk-resize",
    "vm-disk-resize-vmware",
    "vm-edit",
    "vm-info",
    "vm-list",
    "vm-poweron",
    "vm-reboot",
    "vm-rebuild",
    "vm-request-info",
    "vm-request-list",
    "vm-run-script",
]

for item in __all__:
    __import__("clockworkspy.commands.%s" % item)
