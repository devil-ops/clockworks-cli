from clockworkspy.clockworks import SubCommand
from clockworkspy.helpers import pp_results
from datetime import datetime


class Command(SubCommand):
    """
    Test a build of a host, then immediately delete it
    """

    def add_complex_arguments(self, parser, CLOCKWORKS):
        location_ids = CLOCKWORKS.list_location_ids()
        if len(location_ids) > 1:
            default_location = None
        else:
            default_location = location_ids[0]

        image_choices = ['rapid-ubuntu14', 'rapid-ubuntu16', 'ssi-rhel6',
                         'ssi-rhel7']
        parser.add_argument('image', type=str, choices=image_choices,
                            help='Type of image to build')
        parser.add_argument('-f', '--fund-code', required=True,
                            help='Fund Code for server')
        parser.add_argument('-l', '--location', type=str,
                            choices=location_ids, help="Location for VM",
                            default=default_location, required=True)
        return parser

    def run(self, args, CLOCKWORKS):
        start = datetime.now()
        image_flavor, image_dist = args.image.split("-")
        print("Building a test image of %s" % args.image)
        container = None

        if image_flavor == 'rapid':
            import uuid
            hostname = str(uuid.uuid4())
            sysadmin = 'selfadmin'
            fqdn = "%s.oit.duke.edu" % hostname
            container = 'Drew'
            network = '10.236.82.0/24'
            hosting_level = 'bronze'
            if image_dist == 'ubuntu14':
                os_info = CLOCKWORKS.get_os_info_from_name('rapid-ubuntu14')
            elif image_dist == 'ubuntu16':
                os_info = CLOCKWORKS.get_os_info_from_name('rapid-ubuntu16')

            if image_flavor == 'ssi':
                hostname = '%s-%s' % (args.image,
                                      datetime.now().strftime('%s'))
                fqdn = "%s.oit.duke.edu" % hostname
                network = 'private'
                hosting_level = 'bronze'
                sysadmin = '8x5'
                if image_dist == 'rhel7':
                    os_info = CLOCKWORKS.get_os_info_from_name('ssi-rhel7')
                if image_dist == 'rhel6':
                    os_info = CLOCKWORKS.get_os_info_from_name('ssi-rhel6')
            request = CLOCKWORKS.create_vm(
                hostname=fqdn,
                container=container,
                fund_code=args.fund_code,
                os_id=os_info['os_id'],
                template=os_info['template'],
                network=network,
                hosting_level=hosting_level,
                backups=None,
                cpu=1,
                location_id=args.location,
                sysadmin=sysadmin
            )
            pp_results(request)
            if CLOCKWORKS.wait_for_request_to_complete(
                    request['vm_request_id']):
                end = datetime.now()
                length = end - start
                print("Request took %s to build, deleting now" % length)
                info = CLOCKWORKS.get_vm_from_name(fqdn)
                res = CLOCKWORKS.query_path('vms/%s' % info['id'],
                                            method='delete')
                pp_results(res)
            else:
                end = datetime.now()
                length = end - start
                print("Request failed after %s, quitting" % length)
