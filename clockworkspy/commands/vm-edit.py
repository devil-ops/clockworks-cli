from clockworkspy.clockworks import SubCommand


class Command(SubCommand):
    """
    Edit a VM (RAM, CPU, Fund Code, Hosting Level)
    """

    def add_arguments(self, parser):
        hosting_levels = ['platinum', 'silver', 'bronze', 'copper']
        parser.add_argument('target', help='Hostname to edit', nargs='+')
        parser.add_argument(
            '-a', '--allow-poweroff', action='store_true',
            help='Allow clockworks to power off your VM for the change')
        parser.add_argument('-w', '--wait', action='store_true',
                            help='Wait for request to complete')
        parser.add_argument('-r', '-m', '--ram', '--memory', type=int,
                            help='Change the RAM for this host, in Gb')
        parser.add_argument('-c', '--cpu', type=int,
                            help='Change the CPU count for this host')
        parser.add_argument('-f', '--fund-code',
                            help='Change the Fund Code for this host')
        parser.add_argument('-l', '--hosting-level', type=str,
                            choices=hosting_levels,
                            help='Change the Hosting Level for this host')

    def run(self, args, CLOCKWORKS):
        for target in args.target:
            vm = CLOCKWORKS.get_vm_from_name(target)
            print("About to edit %s" % vm['name'])
            payload = {}

            # Make this a string of true/false
            if args.allow_poweroff:
                poweroff_allowed = 'true'
            else:
                poweroff_allowed = 'false'

            payload['poweroff_allowed'] = poweroff_allowed

            if args.ram:
                print("Changing RAM to %s GB" % args.ram)
                payload['ram'] = args.ram

            if args.cpu:
                print("Changing CPU count to %s" % args.cpu)
                payload['cpu'] = args.cpu

            if args.fund_code:
                print("Changing Fund Code %s" % args.fund_code)
                payload['fund_code'] = args.fund_code

            if args.hosting_level:
                print("Changing Hosting level to %s" % args.hosting_level)
                payload['hosting_level'] = args.hosting_level

            request = CLOCKWORKS.query_path('vms/%s' % vm['id'], method='put',
                                            data=payload)
            if args.wait:
                CLOCKWORKS.wait_for_request_to_complete(
                    request['vm_request_id'])
                print("Completed change!")
            else:
                print("Request created: %s" % request['vm_request_id'])
