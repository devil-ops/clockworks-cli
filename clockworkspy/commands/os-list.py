from clockworkspy.clockworks import SubCommand
from clockworkspy.helpers import pp_results


class Command(SubCommand):
    """
    List OSes
    """

    def add_arguments(self, parser):
        return None

    def run(self, args, CLOCKWORKS):
        for vm in CLOCKWORKS.query_path('oses'):
            pp_results(vm)
            print()
