from clockworkspy.clockworks import SubCommand
import pkg_resources


class Command(SubCommand):
    """
    Open up the VMware console
    """

    def run(self, args, CLOCKWORKS):
        print(pkg_resources.get_distribution("clockworks-cli").version)
