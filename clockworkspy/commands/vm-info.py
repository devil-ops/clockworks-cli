from clockworkspy.clockworks import SubCommand
from clockworkspy.helpers import pp_results


class Command(SubCommand):
    """
    Show information on a Clockworks VM
    """

    def add_arguments(self, parser):
        parser.add_argument("target", type=str, help="Hostname to query", nargs="+")
        parser.add_argument(
            "-o", "--open-requests", action="store_true", help="Show Open Requests"
        )
        return parser

    def run(self, args, CLOCKWORKS):
        for target in args.target:
            vm_data = CLOCKWORKS.get_vm_from_name(target)
            pp_results(vm_data)
            print()
            if args.open_requests:
                print("Open Requests:")
                for request in CLOCKWORKS.get_requests_for_host_id(vm_data["id"]):
                    pp_results(request)
                print()
