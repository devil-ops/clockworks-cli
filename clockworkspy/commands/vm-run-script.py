from clockworkspy.clockworks import SubCommand
from devilparser import rcfile
from clockworkspy.vmware import vsphere
import os
import time


class Command(SubCommand):
    """
    Upload and run a script on a VM
    """
    def __init__(self):
        SubCommand.__init__(self)
        self.commands_need_vmware = True

    def add_arguments(self, parser):
        parser.add_argument('target', help='Hostname', nargs='+')
        parser.add_argument('--script', '-s', required=True,
                            help="""
Script to upload and run.  Be sure you have a method to pull guest credentials
in your ~.vmware.yaml file (See README.md for details) """.strip())

        return parser

    def run(self, args, CLOCKWORKS):

        source_script = os.path.expanduser(args.script)
        script_name = os.path.split(source_script)[-1]
        target_script = os.path.join("/root", script_name)

        for target in args.target:
            vm = CLOCKWORKS.get_vm_from_name(target)
            vmware_server = CLOCKWORKS.get_vmware_server_from_mob_url(
                vm['mob_url'])
            vmware_info = rcfile.parse('~/.vmware.yaml').contents()
            VSPHERE = vsphere(vmware_server, vmware_info['username'],
                              vmware_info['password'])
            VSPHERE.upload_file(target, source_script, target_script)
            VSPHERE.run_command(target, '/bin/chmod',
                                '0700 %s' % target_script)

            time.sleep(2)
            VSPHERE.run_command(target, target_script)
