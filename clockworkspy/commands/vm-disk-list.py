from clockworkspy.clockworks import SubCommand
from clockworkspy.helpers import pp_results


class Command(SubCommand):
    """
    List Disks assocatied with the host through Clockworks
    """
    def __init__(self):
        SubCommand.__init__(self)

    def add_arguments(self, parser):
        parser.add_argument('target', help='Hostname', nargs='+')
        return parser

    def run(self, args, CLOCKWORKS):

        for target in args.target:
            print("Retrieving disks for %s" % target)
            vm = CLOCKWORKS.get_vm_from_name(target)
            for disk in vm['disks']:
                pp_results(disk)
                print()
