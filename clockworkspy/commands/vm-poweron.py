from clockworkspy.clockworks import SubCommand
from devilparser import rcfile
from clockworkspy.vmware import vsphere


class Command(SubCommand):
    """
    Reboot a VM using the VSphere API
    """

    def add_arguments(self, parser):
        parser.add_argument('target', help='Hostname of the VM to power on',
                            nargs='+')
        return parser

    def run(self, args, CLOCKWORKS):
        for target in args.target:
            vm = CLOCKWORKS.get_vm_from_name(target)
            vmware_server = CLOCKWORKS.get_vmware_server_from_mob_url(
                vm['mob_url'])
            vmware_info = rcfile.parse('~/.vmware.yaml').contents()
            VSPHERE = vsphere(vmware_server, vmware_info['username'],
                              vmware_info['password'])

            print("Powering on %s" % target)
            VSPHERE.poweron_vm(target)
