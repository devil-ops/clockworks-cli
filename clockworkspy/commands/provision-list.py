from clockworkspy.clockworks import SubCommand
#from clockworkspy.helpers import pp_results
import dateutil


class Command(SubCommand):
    """
    List OSes
    """

    def add_arguments(self, parser):
        return None

    def run(self, args, CLOCKWORKS):
        vm_info = {}
        for vm_request in CLOCKWORKS.query_path('vm_requests'):
            if vm_request['type'] not in ['provision', 'rebuild']:
                continue
            vm_info[vm_request['hostname']] = dateutil.parser.parse(
                vm_request['updated_at'])

        for hostname, provision_time in vm_info.items():
            print(hostname, provision_time.isoformat())
