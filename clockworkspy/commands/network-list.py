from clockworkspy.clockworks import SubCommand
from clockworkspy.helpers import pp_results


class Command(SubCommand):
    """
    List Networks
    """

    def add_arguments(self, parser):
        return None

    def run(self, args, CLOCKWORKS):
        for network in CLOCKWORKS.query_path('networks'):
            if network['type'] == 'unusable':
                continue

            network['locations'] = []
            for location in CLOCKWORKS.query_path('networks/%s/locations' %
                                                  network['subnet']):
                network['locations'].append(location['cluster_name'])

            pp_results(network)
            print()
